from setuptools import setup

setup(
        name='fatnic',
        version='0.1',
        description='Colletion of useful stuff',
        url='https://bitbucket.org/fatnic/fatnic',
        author='Dave Nicholson',
        author_email='fatnic388@gmail.com',
        license='MIT',
        packages=['fatnic',],
        zip_safe=False
        )
