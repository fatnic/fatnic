import os
import shutil
import requests

def supermakedirs(path, mode=0755):
    if not path or os.path.exists(path):
        return []
    (head, tail) = os.path.split(path)
    res = supermakedirs(head, mode)
    os.mkdir(path)
    os.chmod(path, mode)
    res += [path]
    return res


def saveimage(url, filename):
    with open(filename, 'wb') as outfile:
        resp = requests.get(url, stream=True)
        shutil.copyfileobj(resp.raw, outfile)
        del resp
